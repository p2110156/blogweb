/* Debut scrolling : */
window.onscroll = function() {scrollFunction();};
function scrollFunction() {
    if (document.documentElement.scrollTop < 300) {
        document.getElementById("ToTop").classList.remove("backToTop");
        document.getElementById("ToTop").classList.add("backToTopHidden");
    } else {
        document.getElementById("ToTop").classList.remove("backToTopHidden");
        document.getElementById("ToTop").classList.add("backToTop");
    }
}

function topFunction() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
}
/* Fin backToTop */
let emailInput = document.getElementById('email');
let nameInput = document.getElementById('name');
let firstnameInput = document.getElementById('firstname');
let passwordInput = document.getElementById('password');
let confPasswordInput = document.getElementById('conf_password');
let nicknameInput = document.getElementById('nickname');
let cityInput = document.getElementById('city');
let adresseInput = document.getElementById('adresse');
let postalInput = document.getElementById('postal');
/* Debut formulaire block */

function verifyAbonnez() {
    nameInput = document.getElementById('name');
    firstnameInput = document.getElementById('firstname');
    passwordInput = document.getElementById('password');
    confPasswordInput = document.getElementById('conf_password');
    emailInput = document.getElementById('email');
    cityInput = document.getElementById('city');
    adresseInput = document.getElementById('adresse');
    postalInput = document.getElementById('postal');
    verifyAllAbonnez();
}

function verifyAllAbonnez () {
    if (emailInput.value.trim().lengthOf === 0 ||
    nameInput.value.trim().lengthOf === 0 ||
    firstnameInput.value.trim().length === 0 || 
    passwordInput.value.trim().length === 0 || 
    confPasswordInput.value.trim().length === 0 || 
    cityInput.value.trim().length === 0 || 
    adresseInput.value.trim().length === 0 || 
    postalInput.value.trim().length != 5 ||
    /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailInput.value) == false ){
        document.getElementById('button').disabled = true;
        document.getElementById('button').classList.add("buttonNone");
        document.getElementById('button').classList.remove("button");
    }
    else {
        document.getElementById('button').disabled = false;
        document.getElementById('button').classList.remove("buttonNone");
        document.getElementById('button').classList.add("button");
    }
}

function verifyContact() {
    emailInput = document.getElementById('email');
    nameInput = document.getElementById('name');
    firstnameInput = document.getElementById('firstname');
    nicknameInput = document.getElementById('nickname');
    titleInput = document.getElementById('title');
    messageInput = document.getElementById('message');
    verifyAllContact();
}


function verifyAllContact () {
    if (nameInput.value.trim().lengthOf === 0 ||
    firstnameInput.value.trim().lengthOf === 0 ||
    nicknameInput.value.trim().length === 0 || 
    emailInput.value.trim().length === 0 || 
    titleInput.value.trim().length === 0 || 
    messageInput.value.trim().length === 0 ||
    /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailInput.value) == false
    ){
        document.getElementById('button').disabled = true;
        document.getElementById('button').classList.add("buttonNone");
        document.getElementById('button').classList.remove("button");
    }
    else {
        document.getElementById('button').disabled = false;
        document.getElementById('button').classList.remove("buttonNone");
        document.getElementById('button').classList.add("button");
    }
}
/* Fin formulaire block */
